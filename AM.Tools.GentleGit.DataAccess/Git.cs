﻿using AM.Tools.GentleGit.Common;
using LibGit2Sharp;
using System;
using System.Collections.Generic;
using System.Linq;

namespace AM.Tools.GentleGit.DataAccess
{
    public class Git
    {
        public static IEnumerable<string> GetBranches(string gitUrl, CloneOptions options)
        {
            IEnumerable<string> branches = null;

            try
            {
                branches = Repository.ListRemoteReferences(gitUrl, options.CredentialsProvider)?.Where(elem => elem.IsLocalBranch)
                                                                                               ?.Select(elem => elem.CanonicalName
                                                                                               ?.Replace("refs/heads/", ""));
            }
            catch (Exception)
            {
                LogHelper.Log(string.Format(@"Error while fetching branches, URL: {0}", gitUrl));
            }

            return branches;
        }
    }
}
