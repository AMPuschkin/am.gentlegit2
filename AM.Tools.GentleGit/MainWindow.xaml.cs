﻿using AM.Tools.GentleGit.Business;
using AM.Tools.GentleGit.Business.Model;
using AM.Tools.GentleGit.Common;
using AM.Tools.GentleGit.Common.Enum;
using LibGit2Sharp;
using SharpSvn;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Forms;

namespace AM.Tools.GentleGit
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        #region Ctor

        public MainWindow()
        {
            InitializeComponent();
        }

        #endregion

        private void Window_Closing(object sender, CancelEventArgs e)
        {
            e.Cancel = System.Windows.MessageBox.Show(this, "Are you sure?", "Exit", MessageBoxButton.YesNo, MessageBoxImage.Warning, MessageBoxResult.No) == MessageBoxResult.No;
        }

     
        private void CloseApplication_OnClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
      
    }
}
