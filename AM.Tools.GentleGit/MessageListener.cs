﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using GalaSoft.MvvmLight.Messaging;
using AM.Tools.GentleGit.Business.Messages;

namespace AM.Tools.GentleGit
{
    public class MessageListener
    {
        #region constructors and destructors

        public MessageListener()
        {
            InitMessenger();
        }

        #endregion

        #region methods

        private void InitMessenger()
        {
            // Hook to the message that states that some caller wants to open a SettingsWindow.
            Messenger.Default.Register<OpenSettingsWindowMessage>(
                this,
                msg =>
                {
                    Settings settings = new Settings();
                    settings.Show();
                    settings.Topmost = true;
                });

            // Hook to the message that states that some caller wants to open a GitCredWindow.
            Messenger.Default.Register<OpenGitCredWindowMessage>(
                this,
                msg =>
                {
                    GitCredPopUp popUp = new GitCredPopUp();
                    popUp.Show();
                    popUp.Topmost = true;
                });

            Messenger.Default.Register<CloseApplicationMessage>(
                this,
                msg =>
                {
                    
                });
        }
    

        #endregion

        #region properties

        public bool BindableProperty => true;

        #endregion
    }
}
