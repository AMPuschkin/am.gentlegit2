﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using AM.Tools.GentleGit.Common;
using MessageBox = System.Windows.Forms.MessageBox;

namespace AM.Tools.GentleGit
{
    /// <summary>
    /// Interaktionslogik für GitCredPopUp.xaml
    /// </summary>
    public partial class GitCredPopUp : Window
    {
        public GitCredPopUp()
        {
            InitializeComponent();
        }

        private void btnSave(object sender, RoutedEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtUsername.Text) || !string.IsNullOrEmpty(txtPasswort.Password))
            {
                Config.SetGitPassword(txtPasswort.Password);
                Config.SetGitUsername(txtUsername.Text);

                this.Close();
            }
            else
            {
                MessageBox.Show("Username or Password is empty!");
            }
        }

        private void CbShowPassword_OnChecked(object sender, RoutedEventArgs e)
        {
            txtPasswortPlain.Text = txtPasswort.Password;
            txtPasswort.Visibility = Visibility.Collapsed;
            txtPasswortPlain.Visibility = Visibility.Visible;
        }

        private void CbShowPassword_OnUnchecked(object sender, RoutedEventArgs e)
        {
            txtPasswort.Visibility = Visibility.Visible;
            txtPasswortPlain.Visibility = Visibility.Collapsed;
        }

        private void BtnDialogClose_OnClick(object sender, RoutedEventArgs e)
        {
            this.Close();
        }
    }
}
