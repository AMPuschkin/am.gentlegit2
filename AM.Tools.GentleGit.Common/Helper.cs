﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using AM.Tools.GentleGit.Common.Enum;
using Bluepond.Net.Core;
using Interop.AutoItX3Lib;


namespace AM.Tools.GentleGit.Common
{
    public class Helper
    {
        public static string GetApplicationSnvFolderNameByCompany(string companyName)
        {
            if (companyName.StartsWith("DTS_") ||
                companyName.StartsWith("DTTS_") ||
                companyName.StartsWith("DT_") ||
                companyName.StartsWith("DeutscheTelekom") ||
                companyName.StartsWith("Deutsche Telekom"))
            {
                return "DTAG";
            }

            return $"{companyName}Applications";
        }

        public static string GetApplicationGitRepoNameByCompany(string companyName)
        {
            if (companyName.StartsWith("DTS_") ||
                companyName.StartsWith("DT_") ||
                companyName.StartsWith("DeutscheTelekom") ||
                companyName.StartsWith("Deutsche Telekom"))
            {
                return ".DTAG.Applications";
            }

            return $".{companyName}.Applications";
        }

        public static string GetApplicationDestinationPathByCompany(string companyName)
        {
            if (companyName.StartsWith("DTS_") ||
                companyName.StartsWith("DT_") ||
                companyName.StartsWith("DeutscheTelekom") ||
                companyName.StartsWith("Deutsche Telekom"))
            {
                return $@"\DTAG";
            }

            return $@"\{companyName}Applications";
        }

        public static string GetProcessesGitRepoNameByCompany(string companyName)
        {
            if (companyName.StartsWith("DTS_") ||
                companyName.StartsWith("DT_") ||
                companyName.StartsWith("DeutscheTelekom") ||
                companyName.StartsWith("Deutsche Telekom"))
            {
                return "." + GetTelekomCompanyNameShort(companyName) + ".Processes"; 
            }

            return $".{companyName}.Processes";
        }

        public static string GetConfigurationGitRepoNameByCompany(string companyName)
        {
            if (companyName.StartsWith("DTS_") ||
                companyName.StartsWith("DT_") ||
                companyName.StartsWith("DeutscheTelekom") ||
                companyName.StartsWith("Deutsche Telekom"))
            {
                return "." + GetTelekomCompanyNameShort(companyName)+ ".Configuration"; 
            }

            return $".{companyName}.Configuration";
        }

        public static string GetTelekomCompanyNameShort(string companyNameLong)
        {
            if (companyNameLong.StartsWith("Deutsche Telekom"))
            {
                return "DTTS";
            }else if (companyNameLong.StartsWith("DeutscheTelekom"))
            {
                return "DTKS";
            }

            return "";

        }

        public static string GetCompanyBranchName(string companyName)
        {
            if (companyName.StartsWith("DeutscheTelekom") ||
                companyName.StartsWith("Deutsche Telekom"))
            {
                return GetTelekomCompanyNameShort(companyName) ;
            }

            return companyName;
        }

        public static string GetGitBranchName(string company, string projectname)
        {
            if (projectname.StartsWith("Prod"))
            {
                projectname = projectname.Replace("Prod", "");
            }

            var CompanyName = Helper.GetCompanyBranchName(company);

            return "prod/" + CompanyName + "/" + projectname;
        }

        public static string GetProjectNameFromBranch(string branchName, string companyName)
        {
            return branchName.Replace("prod/" + GetCompanyBranchName(companyName) + "/", "");
        }

        /// <summary>
        /// Compare the passed item with all of the names which aren't allowed to be projectnames
        /// if item is null or whitespace, return false
        /// if item is in the array of projectnames, return false
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static bool IsValidCompany(string item)
        {
            string[] disallowedProjectNames = Config.GetDisallowedProjectNames();

            return !string.IsNullOrWhiteSpace(item) &&
                   (disallowedProjectNames == null || !disallowedProjectNames.Contains(item) && !item.Contains("Application"));
        }

        /// <summary>
        /// Compare the passed item with all of the names which aren't allowed to be foldernames
        /// if item is null or whitespace, return false
        /// if item is in the array of foldernames, return false
        /// </summary>
        /// <param name="item"></param>
        /// <returns></returns>
        public static bool IsValidFolder(string item)
        {
            string[] disallowedFolderNames = Config.GetDisallowedFolderNames();

            return !string.IsNullOrWhiteSpace(item) &&
                   (disallowedFolderNames == null || !disallowedFolderNames.Contains(item));
        }

        /// <summary>
        /// Check if the passed project is in the collection of projects in git(common for now)
        /// </summary>
        /// <param name="selectedItem"></param>
        /// <param name="gitBranches"></param>
        /// <returns></returns>
        public static bool IsProjectOnGit(string selectedItem, string[] gitBranches)
        {
            bool? isProjectOnGit = false;

            if (!string.IsNullOrWhiteSpace(selectedItem))
            {
                isProjectOnGit = gitBranches?.Any(i => i.Equals(selectedItem));
            }

            return isProjectOnGit.HasValue && isProjectOnGit.Value;
        }

        /// <summary>
        /// If this directory doesn't exist it is empty for sure
        /// If this directory exists, check if it has files in it
        /// If this directory exists check if there are more directories which we then can check further recursively
        /// </summary>
        /// <param name="path"></param>
        /// <returns></returns>
        public static bool IsDirectoryEmpty(string path)
        {
            if (!Directory.Exists(path))
            {
                return true;
            }

            if (Directory.GetFiles(path).Length > 0)
            {
                return false;
            }

            foreach (string directory in Directory.GetDirectories(path))
            {
                if (!IsDirectoryEmpty(directory))
                {
                    return false;
                }
            }

            return true;
        }


        public static String Encrypt(String plaintext, String password)
        {
            

            return Encryption.Encrypt(plaintext, password);
        }

        public static String Decrypt(String ciphertext, String password)
        {
            return Encryption.Decrypt(ciphertext, password);
        }

        public static void DirectoryCopy(
            string sourceDirName, string destDirName, bool copySubDirs)
        {
            DirectoryInfo dir = new DirectoryInfo(sourceDirName);
            DirectoryInfo[] dirs = dir.GetDirectories();

            // If the source directory does not exist, throw an exception.
            if (!dir.Exists)
            {
                throw new DirectoryNotFoundException(
                    "Source directory does not exist or could not be found: "
                    + sourceDirName);
            }

            // If the destination directory does not exist, create it.
            if (!Directory.Exists(destDirName))
            {
                Directory.CreateDirectory(destDirName);
            }


            // Get the file contents of the directory to copy.
            FileInfo[] files = dir.GetFiles();

            foreach (FileInfo file in files)
            {
                // Create the path to the new copy of the file.
                string temppath = Path.Combine(destDirName, file.Name);

                // Copy the file.
                file.CopyTo(temppath, false);
            }

            // If copySubDirs is true, copy the subdirectories.
            if (copySubDirs)
            {

                foreach (DirectoryInfo subdir in dirs)
                {
                    // Create the subdirectory.
                    string temppath = Path.Combine(destDirName, subdir.Name);

                    // Copy the subdirectories.
                    DirectoryCopy(subdir.FullName, temppath, copySubDirs);
                }
            }
        }

    }
}
