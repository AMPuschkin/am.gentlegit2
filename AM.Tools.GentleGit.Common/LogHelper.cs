﻿using System;
using System.IO;

namespace AM.Tools.GentleGit.Common
{
    public class LogHelper
    {
        private LogHelper()
        {
        }
        private static volatile LogHelper instance;
        private static object syncRoot = new Object();

        public static LogHelper Instance
        {
            get
            {
                if (instance == null)
                {
                    lock (syncRoot)
                    {
                        if (instance == null)
                            instance = new LogHelper();
                    }
                }

                return instance;
            }
        }

        public  void Log(string message, Exception ex = null)
        {
            string configFilePath = Config.GetLogFilePath();
            string filePath = !string.IsNullOrWhiteSpace(configFilePath) ? configFilePath : "Log.txt";
            System.IO.Directory.CreateDirectory(Path.GetFullPath(filePath));
            filePath += "log.txt";
            if (!File.Exists(filePath))
            {
                File.Create(filePath).Close();
            }
            else
            {
                using (StreamWriter sw = File.AppendText(filePath))
                {
                    DateTime dateTimeNow = DateTime.Now;

                    sw.WriteLine($"{dateTimeNow} - Message: {message}");

                    if (ex != null)
                    {
                        sw.WriteLine($"{dateTimeNow} - Exception message: {ex.Message}");
                        sw.WriteLine($"{dateTimeNow} - InnerException message: {(ex.InnerException != null ? ex.InnerException.Message : "Empty")}");
                    }

                    sw.WriteLine("---------------------------------------");
                    sw.WriteLine();
                }
            }
           
        }
    }
}
