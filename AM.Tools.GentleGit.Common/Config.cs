﻿using System;
using System.Configuration;
using System.Net.Mime;

namespace AM.Tools.GentleGit.Common
{
    public class Config
    {
        public static string GetGitUrl()
        {
            return ConfigurationManager.AppSettings["GitUrl"];
        }

        public static void SetGitUrl(String url)
        {
            SetSetting("GitUrl", url);
        }

        public static string GetSvnUrl()
        {
            return ConfigurationManager.AppSettings["SvnUrl"];
        }

        public static void SetSvnUrl(String url)
        {
            SetSetting("SvnUrl", url);
        }

        public static string GetLogFilePath()
        {
            return ConfigurationManager.AppSettings["LogFilePath"];
        }

        public static void SetLogFilePath(String path)
        {
            SetSetting("LogFilePath", path);
        }

        public static string[] GetDisallowedProjectNames()
        {
            return ConfigurationManager.AppSettings["DisallowedProjectNames"]?.Split(';');
        }

        public static string[] GetDisallowedFolderNames()
        {
            return ConfigurationManager.AppSettings["DisallowedFolderNames"]?.Split(';');
        }

        public static string GetGitUsername()
        {
            return ConfigurationManager.AppSettings["GitUsername"];
        }

        public static string GetGitPassword()
        {
            String cipherPw = ConfigurationManager.AppSettings["GitPassword"];
            if (string.IsNullOrEmpty(cipherPw))
            {
                return "";
            }
            return Helper.Decrypt(cipherPw, Constants.gitCredEncryptioPW);
        }


        public static void SetGitPassword(String plainpw)
        {
            SetSetting("GitPassword", Helper.Encrypt(plainpw, Constants.gitCredEncryptioPW));
        }

        public static void SetGitUsername(String username)
        {
            SetSetting("GitUsername", username);
        }

        public static bool IsGitPopUpShown()
        {
            return Boolean.Parse(ConfigurationManager.AppSettings["GitPopUpShown"]);
        }

        public static bool IsGitConfigCloned()
        {
            return Boolean.Parse(ConfigurationManager.AppSettings["GitConfigRepoCloned"]);
        }

        public static void SetGitPopUpShown(bool value)
        {
            SetSetting("GitPopUpShown", value.ToString());
        }
        public static void SetGitConfigCloned(bool value)
        {
            SetSetting("GitConfigRepoCloned", value.ToString());
        }

        private static void SetSetting(string key, string value)
        {
            Configuration configuration =
                ConfigurationManager.OpenExeConfiguration(ConfigurationUserLevel.None);
            configuration.AppSettings.Settings[key].Value = value;
            configuration.Save(ConfigurationSaveMode.Full, true);
            ConfigurationManager.RefreshSection("appSettings");
        }
    }
}
