﻿namespace AM.Tools.GentleGit.Common.Enum
{
    public enum ProjectSettingsEnum
    {
        Undefined = 0,
        SvnConfiguration = 1,
        SvnProcess = 2,
        SvnCommon = 3,
        SvnApplications = 4,
        GitConfiguration = 5,
        GitProcess = 6,
        GitCommon = 7,
        GitApplications = 8,
        SvnCreateInputData = 9,
        SvnRename = 10,
        GitCreateInputData = 11,
        GitRename = 12
    }
}
