using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Forms;
using System.Windows.Input;
using AM.Tools.GentleGit.Business.CommandsHelper;
using AM.Tools.GentleGit.Business.Messages;
using AM.Tools.GentleGit.Business.Model;
using AM.Tools.GentleGit.Common;
using AM.Tools.GentleGit.Common.Enum;
using GalaSoft.MvvmLight;
using LibGit2Sharp;
using SharpSvn;



namespace AM.Tools.GentleGit.Business.ViewModel
{
    /// <summary>
    /// This class contains properties that the main View can data bind to.
    /// <para>
    /// Use the <strong>mvvminpc</strong> snippet to add bindable properties to this ViewModel.
    /// </para>
    /// <para>
    /// You can also use Blend to data bind with the tool's support.
    /// </para>
    /// <para>
    /// See http://www.galasoft.ch/mvvm
    /// </para>
    /// </summary>
    public class MainViewModel : ViewModelBase
    {
        /// <summary>
        /// Initializes a new instance of the MainViewModel class.
        /// </summary>
        public MainViewModel()
        {
            ////if (IsInDesignMode)
            ////{
            ////    // Code runs in Blend --> create design time data.
            ////}
            ////else
            ////{
            ////    // Code runs "for real"
            ////}

            bool result = GitManager.GetOrUpdateConfigFile(Config.GetGitUrl()+ ".ApplicationConfiguration");
            if (result)
            {
                var customers =  GitManager.GetCustomersList();
                if (customers != null)
                {
                    Companys = customers;
                }
            }
            Destination = $@"C:\AM Process\";
            CheckOutRB = true;
            ConnectToSVN();

            svnApplicationController = new SvnManager();
            svnApplicationController.ProgressEvent += SvnApplicationsController_ProgressEvent;

            svnCommonController = new SvnManager();
            svnCommonController.ProgressEvent += SvnCommonController_ProgressEvent;

            //svnConfigurationController = new SvnManager();
            //svnConfigurationController.ProgressEvent += SvnConfigurationController_ProgressEvent;

            svnProcessController = new SvnManager();
            svnProcessController.ProgressEvent += SvnProcessController_ProgressEvent;

            gitApplicationController = new GitManager();
            gitApplicationController.ProgressEvent += GitApplicationController_ProgressEvent;
            gitApplicationController.PushProgressEvent += GitApplicationController_PushProgressEvent;

            gitCommonController = new GitManager();
            gitCommonController.ProgressEvent += GitCommonController_ProgressEvent;
            gitCommonController.PushProgressEvent += GitCommonController_PushProgressEvent;

            gitConfigurationController = new GitManager();
            gitConfigurationController.ProgressEvent += GitConfigurationController_ProgressEvent;
            gitConfigurationController.PushProgressEvent += GitConfigurationController_PushProgressEvent;

            gitProcessController = new GitManager();
            gitProcessController.ProgressEvent += GitProcessController_ProgressEvent;
            gitProcessController.PushProgressEvent += GitProcessController_PushProgressEvent;

            OpenGitCredCommand =  new RelayCommand((a) => MessengerInstance.Send(new OpenGitCredWindowMessage()));
            OpenSettingsCommand = new RelayCommand((a) => MessengerInstance.Send(new OpenSettingsWindowMessage()));

            if (!Config.IsGitPopUpShown())
            {
                MessengerInstance.Send(new OpenGitCredWindowMessage());
                Config.SetGitPopUpShown(true);
            }
            else if (string.IsNullOrEmpty(Config.GetGitPassword()) || string.IsNullOrEmpty(Config.GetGitUsername()))
            {
                MessengerInstance.Send(new OpenGitCredWindowMessage());
            }

            ButtonEnabled = true;
            CheckoutProject_Click();
        }

        #region Const

        private bool isConnected;
        private bool _GitConfiguartionCreated;
        private bool _GitProcessCreated;
        private static volatile int _runningThreadCounter;

        private readonly SvnManager svnApplicationController;
        private readonly SvnManager svnCommonController;
        //private readonly SvnManager svnConfigurationController;
        private readonly SvnManager svnProcessController;

        private readonly GitManager gitApplicationController;
        private readonly GitManager gitCommonController;
        private readonly GitManager gitConfigurationController;
        private readonly GitManager gitProcessController;
        /// <summary>
        /// Opens a new setting window.
        /// </summary>
        public RelayCommand OpenSettingsCommand { get; private set; }
        /// <summary>
        /// Opens a new git cred window.
        /// </summary>
        public RelayCommand OpenGitCredCommand { get; private set; }
        /// <summary>
        /// Close the Application
        /// </summary>
        public RelayCommand CloseApplicationCommand { get; private set; }

        #endregion

        public bool CheckOutRB { get; set; }
        public bool CreateNewProjectRB { get; set; }
        public int GitOptionsSpan { get; set; }
        public List<String> Companys { get; set; }
        public int SelectedCompanyIndex { get; set; }
      
        public String SelectedCompany { get; set; }
        public int SelectedFolderIndex { get; set; }
        public String SelectedFolder  { get; set; }
        public int SelectedProjectIndex { get; set; }
        public String SelectedProject { get; set; }
        public List<String> FolderNames { get; set; }
        public List<String> CheckOutProjects { get; set; }

        public String Destination { get; set; }
        public String NewProjectName { get; set; }

        public bool GitConfiguration { get; set; }
        public bool GitProcess { get; set; }
        public bool GitCommon { get; set; }
        public bool GitApplications { get; set; }
        public bool GitCreateInputdata { get; set; }
        public bool GitRename { get; set; }
        public bool SvnCommon { get; set; }
        public bool SvnProcess { get; set; }
        public bool SvnApplications { get; set; }

        public bool GitProcessEnabled { get; set; }
        public bool GitCommonEnabled { get; set; }
        public bool GitConfigurationEnabled { get; set; }
        public bool GitApplicationsEnabled { get; set; }
     
        public bool StatusVisible { get; set; }
     
        public bool SvnCommonEnabled { get; set; }
        public bool SvnProcessEnabled { get; set; }
        public bool SvnApplicationsEnabled { get; set; }
        public bool ButtonEnabled { get; set; }
        public bool NewProjectNameEnabled { get; set; }
        public bool GitRenameEnabled { get; set; }
        public bool DdlCompanysEnabled { get; set; }
        public bool DdlFoldersEnabled { get; set; }
        public bool DdlProjectsEnabled { get; set; }
        public bool ChooseFolderEnabled { get; set; }
        public bool DestinationEnabled { get; set; }
        public String LabelGitConfiguration { get; set; }
        public String LabelGitProcess { get; set; }
        public String LabelGitCommon { get; set; }
        public String LabelGitApplications { get; set; }
        public String LabelGitCreateInputdata { get; set; }
        public String LabelGitRename { get; set; }
        public String LabelSvnCommon { get; set; }
        public String LabelSvnProcess { get; set; }
        public String LabelSvnApplications { get; set; }
        public String ButtonText { get; set; }



        /// <summary>
        /// Check the SVN Connection
        /// Select the first company and activate all other buttons/fields if the list isn't empty
        /// 
        /// Call it without the button?
        /// </summary>
        private void ConnectToSVN()
        {
            string svnUrl = Config.GetSvnUrl();

            if (!string.IsNullOrWhiteSpace(svnUrl))
            {
                StatusVisible = true;

                string[] svnBranches = SvnManager.GetBranches(svnUrl);

                if (svnBranches != null && svnBranches.Any())
                {
                    //System.Windows.MessageBox.Show("Connected", "Info");

                    //var Companys2 = svnBranches.Where(Helper.IsValidCompany) ;
                    //Companys = Companys2.ToList();
                    //SelectedCompanyIndex = 0;

                    ToggleControlsStatus(isEnabled: true);

                    isConnected = true;
                    ButtonEnabled = true;
                    AddGitProcessBranchesToList();
                }
                else
                {
                    System.Windows.MessageBox.Show("Svn connection failed");

                    ToggleControlsStatus(isEnabled: false);
                }

                StatusVisible = false;
            }
            else
            {
                //System.Windows.MessageBox.Show("SvnUrl is empty!");
            }
        }

       
        /// <summary>
        /// Add the Git Process Branches from Company to Folder "Git Projects"
        /// </summary>
        private void AddGitProcessBranchesToList()
        {
            if (CheckOutRB && !string.IsNullOrEmpty(SelectedFolder))
            {
                string gitUrl = Config.GetGitUrl();
                var CompanyName = SelectedCompany;
                if (SelectedFolder.Equals("Git Projects"))
                {
                    if (!String.IsNullOrEmpty(CompanyName))
                    {
                        string[] gitBranchesProcesses =
                            GitManager.GetBranches(gitUrl + Helper.GetProcessesGitRepoNameByCompany(CompanyName));
                        if (gitBranchesProcesses != null)
                        {
                            List<string> itemsSource = new List<String>();
                            foreach (string branchName in gitBranchesProcesses)
                            {
                                if (!branchName.Contains("feature/") && !branchName.Contains("master"))
                                {
                                    var project = "Prod" + Helper.GetProjectNameFromBranch(branchName, CompanyName);
                                    itemsSource.Add(project);
                                }

                            }
                            CheckOutProjects = itemsSource;
                            SelectedProjectIndex = 0;
                        }
                        else
                        {
                            CheckOutProjects = new List<String>();
                        }

                    }
                }

            }
        }



        /// <summary>
        /// Set the BackgroundWorker Argument for CreateNewProject, and start Checkout
        /// </summary>
        /// <param name="projectSettings"></param>
        private void CreateNewProject(ProjectSettings projectSettings)
        {
            string svnUrl = projectSettings.SvnUrl;
            string gitUrl = projectSettings.GitUrl;
            string projectName = projectSettings.ProjectName;

            if (!string.IsNullOrWhiteSpace(svnUrl) && !string.IsNullOrWhiteSpace(gitUrl))
            {
                StatusVisible = true;
                ButtonText = "Creating new project";

                if (projectSettings.Options.Contains(ProjectSettingsEnum.GitCommon))
                {
                    BgwArguments bgwArguments = new BgwArguments
                    {
                        Url = gitUrl + ".Common",
                        DestinationPath = projectSettings.ApplicationPath + "/Common",
                        ActiveOption = ProjectSettingsEnum.GitCommon,
                        ProjectName = projectName,
                        CreateNewProjectOnGit = true,
                        ProjectSettings = projectSettings
                    };

                    CheckOut(bgwArguments);
                }
                if (projectSettings.Options.Contains(ProjectSettingsEnum.GitApplications))
                {
                    BgwArguments bgwArguments = new BgwArguments
                    {
                        Url = gitUrl + Helper.GetApplicationGitRepoNameByCompany(projectSettings.CompanyName),
                        DestinationPath = projectSettings.ApplicationPath + Helper.GetApplicationDestinationPathByCompany(projectSettings.CompanyName),
                        ActiveOption = ProjectSettingsEnum.GitApplications,
                        ProjectName = projectName,
                        CreateNewProjectOnGit = true,
                        ProjectSettings = projectSettings
                    };

                    CheckOut(bgwArguments);
                }
                if (projectSettings.Options.Contains(ProjectSettingsEnum.GitConfiguration))
                {
                    BgwArguments bgwArguments = new BgwArguments
                    {
                        Url = gitUrl + Helper.GetConfigurationGitRepoNameByCompany(projectSettings.CompanyName),
                        DestinationPath = projectSettings.ApplicationPath + $@"\Configuration",
                        ActiveOption = ProjectSettingsEnum.GitConfiguration,
                        ProjectName = projectName,
                        CreateNewProjectOnGit = true,
                        ProjectSettings = projectSettings
                    };

                    CheckOut(bgwArguments);
                }
                if (projectSettings.Options.Contains(ProjectSettingsEnum.GitProcess))
                {
                    BgwArguments bgwArguments = new BgwArguments
                    {
                        Url = gitUrl + Helper.GetProcessesGitRepoNameByCompany(projectSettings.CompanyName),
                        DestinationPath = projectSettings.ApplicationPath + $@"\Process",
                        ActiveOption = ProjectSettingsEnum.GitProcess,
                        ProjectName = projectName,
                        CreateNewProjectOnGit = true,
                        ProjectSettings = projectSettings
                    };

                    CheckOut(bgwArguments);
                }
            }
        }

        /// <summary>
        /// Set the BackgroundWorker Argument for Checkout, and start Checkout
        /// </summary>
        /// <param name="projectSettings"></param>
        private void CheckOutProject(ProjectSettings projectSettings)
        {
            BgwArguments bgwArguments = null;

            string applicationPath = projectSettings.ApplicationPath;
            string svnUrl = projectSettings.SvnUrl;
            string gitUrl = projectSettings.GitUrl;
            string companyName = projectSettings.CompanyName;
            string folderName = projectSettings.FolderName;
            string projectName = projectSettings.ProjectName;
            string applicationFoldername = Helper.GetApplicationSnvFolderNameByCompany(companyName);
            List<ProjectSettingsEnum> options = projectSettings.Options;

            if (options.Contains(ProjectSettingsEnum.SvnCommon))
            {
                //special case for VW Common is in trunk VW folder
                bgwArguments = new BgwArguments
                {
                    Url = companyName.Equals("VW") ? $@"{svnUrl}/Common/{folderName}VW/{projectName}" : $@"{svnUrl}/Common/{folderName}/{projectName}",
                    DestinationPath = $@"{applicationPath}\Common",
                    ActiveOption = ProjectSettingsEnum.SvnCommon,
                };

                CheckOut(bgwArguments);
            }

            if (options.Contains(ProjectSettingsEnum.SvnProcess))
            {
                bgwArguments = new BgwArguments
                {
                    Url = $@"{svnUrl}/{companyName}/{folderName}/{projectName}",
                    DestinationPath = $@"{applicationPath}\Process",
                    ActiveOption = ProjectSettingsEnum.SvnProcess,
                    ProjectSettings = projectSettings
                };

                CheckOut(bgwArguments);
            }

            if (options.Contains(ProjectSettingsEnum.SvnApplications))
            {


                bgwArguments = new BgwArguments
                {
                    Url = $@"{svnUrl}/{applicationFoldername}/{folderName}/{projectName}",
                    DestinationPath = $@"{applicationPath}\{applicationFoldername}",
                    ActiveOption = ProjectSettingsEnum.SvnApplications,
                };

                CheckOut(bgwArguments);
            }

            if (options.Contains(ProjectSettingsEnum.SvnConfiguration))
            {
                bgwArguments = new BgwArguments
                {
                    Url = $@"{svnUrl}/{companyName}/trunk/Configuration",
                    DestinationPath = $@"{applicationPath}\Configuration",
                    ActiveOption = ProjectSettingsEnum.SvnConfiguration,
                };

                CheckOut(bgwArguments);
            }

            if (options.Contains(ProjectSettingsEnum.GitCommon) && !projectSettings.CreateNewProject) // Checkout from Git, if not new created 
            {
                bgwArguments = new BgwArguments
                {
                    Url = gitUrl + ".Common",
                    DestinationPath = $@"{applicationPath}\Common",
                    ProjectName = projectName,
                    ActiveOption = ProjectSettingsEnum.GitCommon,
                };

                CheckOut(bgwArguments);
            }

            if (options.Contains(ProjectSettingsEnum.GitApplications) && !projectSettings.CreateNewProject) // Checkout from Git, if not new created 
            {
                bgwArguments = new BgwArguments
                {
                    Url = gitUrl + Helper.GetApplicationGitRepoNameByCompany(projectSettings.CompanyName),
                    DestinationPath = $@"{applicationPath}" + Helper.GetApplicationDestinationPathByCompany(projectSettings.CompanyName),
                    ProjectName = projectName,
                    ActiveOption = ProjectSettingsEnum.GitApplications,
                };

                CheckOut(bgwArguments);
            }

            if (options.Contains(ProjectSettingsEnum.GitConfiguration) && !projectSettings.CreateNewProject) // Checkout from Git, if not new created 
            {
                bgwArguments = new BgwArguments
                {
                    Url = gitUrl + Helper.GetConfigurationGitRepoNameByCompany(projectSettings.CompanyName),
                    DestinationPath = $@"{applicationPath}\Configuration",
                    ProjectName = "master", //ONLY THE MASTER BRANCH
                    ActiveOption = ProjectSettingsEnum.GitConfiguration,
                };

                CheckOut(bgwArguments);
            }

            if (options.Contains(ProjectSettingsEnum.GitProcess) && !projectSettings.CreateNewProject) // Checkout from Git, if not new created 
            {
                bgwArguments = new BgwArguments
                {
                    Url = gitUrl + Helper.GetProcessesGitRepoNameByCompany(projectSettings.CompanyName),
                    DestinationPath = $@"{applicationPath}\Process",
                    ProjectName = projectName,
                    ActiveOption = ProjectSettingsEnum.GitProcess,
                };

                CheckOut(bgwArguments);
            }
        }

        /// <summary>
        /// Check if DestinationFolder is empty and start checkout async with Background
        /// </summary>
        /// <param name="bgwArguments"></param>
        private void CheckOut(BgwArguments bgwArguments)
        {
            StatusVisible = true;

            ButtonEnabled = false;

            bool isDestinationFolderEmpty = Helper.IsDirectoryEmpty(bgwArguments.DestinationPath);

            if (!isDestinationFolderEmpty)
            {
                DialogResult result = System.Windows.Forms.MessageBox.Show($"Folder '{bgwArguments.DestinationPath}' is not empty! Do you want to make a checkout anyway?", "Checkout Folder exist", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);

                if (result != System.Windows.Forms.DialogResult.Yes)
                {
                    return;
                }
            }

            BackgroundWorker backgroundWorker = new BackgroundWorker();
            backgroundWorker.DoWork += BgWorker_DoWork; ;
            backgroundWorker.WorkerSupportsCancellation = true;

            backgroundWorker.RunWorkerAsync(bgwArguments);
        }

        /// <summary>
        /// Start the Checkout or CreateNewProject Process at the Controllers
        /// </summary>
        /// <param name="arguments"></param>
        private void StartCheckOutProcess(BgwArguments arguments)
        {
            string repositoryUrl = arguments.Url;
            string destination = arguments.DestinationPath;
            string projectName = arguments.ProjectName;
            string gitBranchName = Helper.GetGitBranchName(arguments.ProjectSettings.CompanyName, projectName);

            ProjectSettingsEnum activeOption = arguments.ActiveOption;

            switch (activeOption)
            {
                case ProjectSettingsEnum.SvnCommon:
                    svnCommonController.CheckOut(repositoryUrl, destination);
                    break;
                case ProjectSettingsEnum.SvnApplications:
                    svnApplicationController.CheckOut(repositoryUrl, destination);
                    break;
                case ProjectSettingsEnum.SvnProcess:
                    svnProcessController.CheckOut(repositoryUrl, destination);
                    break;
                //case ProjectSettingsEnum.SvnConfiguration:
                //    svnConfigurationController.CheckOut(repositoryUrl, destination);
                //    break;
                case ProjectSettingsEnum.GitCommon:
                    if (arguments.CreateNewProjectOnGit)
                    {
                        gitCommonController.CreateNewProject(repositoryUrl, destination, gitBranchName);
                    }
                    else
                    {
                        gitCommonController.CheckOut(repositoryUrl, destination, gitBranchName);
                    }
                    break;
                case ProjectSettingsEnum.GitConfiguration:

                    _GitConfiguartionCreated = gitConfigurationController.CheckOut(repositoryUrl, destination, "master"); //ONLY THE MASTER BRANCH

                    break;
                case ProjectSettingsEnum.GitApplications:
                    if (arguments.CreateNewProjectOnGit)
                    {
                        gitApplicationController.CreateNewProject(repositoryUrl, destination, gitBranchName);
                    }
                    else
                    {
                        gitApplicationController.CheckOut(repositoryUrl, destination, gitBranchName);
                    }
                    break;
                case ProjectSettingsEnum.GitProcess:
                    if (arguments.CreateNewProjectOnGit)
                    {
                        _GitProcessCreated = gitProcessController.CreateNewProject(repositoryUrl, destination, gitBranchName);

                    }
                    else
                    {
                        gitProcessController.CheckOut(repositoryUrl, destination, gitBranchName);
                    }

                    break;
            }
        }

        /// <summary>
        /// Check if Files are Ready to Rename and starts RenameProcessfiles
        /// </summary>
        /// <param name="projectSettings"></param>
        private void CheckIfReadyToRenameFiles(ProjectSettings projectSettings)
        {

            List<ProjectSettingsEnum> options = projectSettings.Options;
            if (options.Contains(ProjectSettingsEnum.GitRename))
            {
                if (options.Contains(ProjectSettingsEnum.GitConfiguration) &&
                    options.Contains(ProjectSettingsEnum.GitProcess))
                {
                    if (_GitProcessCreated && _GitConfiguartionCreated)
                    {
                        RenameProcessFiles(projectSettings);
                        StatusVisible = false;
                        ButtonEnabled = true;
                    }
                }
                else if (options.Contains(ProjectSettingsEnum.GitProcess))
                {
                    if (_GitProcessCreated)
                    {
                        StatusVisible = false;
                        ButtonEnabled = true;
                        RenameProcessFiles(projectSettings);
                    }

                }
                else if (options.Contains(ProjectSettingsEnum.GitConfiguration))
                {
                    if (_GitConfiguartionCreated)
                    {
                        StatusVisible = false;
                        ButtonEnabled = true;
                        RenameProcessFiles(projectSettings);
                    }
                }
            }
        }

        /// <summary>
        /// Rename Inputdata and SLN files if ProjectSettingsEnum.GitRename was checked
        /// </summary>
        /// <param name="projectSettings"></param>
        private void RenameProcessFiles(ProjectSettings projectSettings)
        {

            List<ProjectSettingsEnum> options = projectSettings.Options;
            string applicationPath = projectSettings.ApplicationPath;
            string companyName = projectSettings.CompanyName;
            string projectName = projectSettings.ProjectName;
            String projectNameInputdata = projectName; //Without 'Prod'
            string trimmedCompanyName = companyName.Replace(" ", "");

            if (options.Contains(ProjectSettingsEnum.GitCreateInputData))
            {
                var computerUrl = $@"{applicationPath}\Configuration\InputData\";
                Helper.DirectoryCopy(computerUrl + "AM.Net.Company.Example.Data", computerUrl + $"AM.Net.{trimmedCompanyName}.{projectNameInputdata}.Data", true);

                if (projectSettings.Options.Contains(ProjectSettingsEnum.GitRename))
                {
                    File.Move(computerUrl + $"AM.Net.{trimmedCompanyName}.{projectNameInputdata}.Data/AM.Net.Company.Example.Data.csproj"
                        , computerUrl + $"AM.Net.{trimmedCompanyName}.{projectNameInputdata}.Data/AM.Net.{trimmedCompanyName}.{projectNameInputdata}.Data.csproj");

                    File.Move(computerUrl + $"AM.Net.{trimmedCompanyName}.{projectNameInputdata}.Data/ExampleInputData.cs"
                        , computerUrl + $"AM.Net.{trimmedCompanyName}.{projectNameInputdata}.Data/{projectNameInputdata}InputData.cs");

                    File.Move(computerUrl + $"AM.Net.{trimmedCompanyName}.{projectNameInputdata}.Data/ExampleOutputData.cs"
                        , computerUrl + $"AM.Net.{trimmedCompanyName}.{projectNameInputdata}.Data/{projectNameInputdata}OutputData.cs");
                }
            }

            var baseUrl = $@"{applicationPath}\Process";
            //MOVE Process Folder and Files
            //Trigger
            Directory.Move($@"{baseUrl}\AM.Net.Company.Example.Dco.Trigger", $@"{baseUrl}\AM.Net.{trimmedCompanyName}.{projectName}.Dco.Trigger");
            File.Move($@"{baseUrl}\AM.Net.{trimmedCompanyName}.{projectName}.Dco.Trigger\AM.Net.Company.Example.Dco.Trigger.csproj", $@"{baseUrl}\AM.Net.{trimmedCompanyName}.{projectName}.Dco.Trigger\AM.Net.{trimmedCompanyName}.{projectName}.Dco.Trigger.csproj");

            //Process
            Directory.Move($@"{baseUrl}\AM.Net.Company.Example.Pro", $@"{baseUrl}\AM.Net.{trimmedCompanyName}.{projectName}.Pro");
            File.Move($@"{baseUrl}\AM.Net.{trimmedCompanyName}.{projectName}.Pro\AM.Net.Company.Example.Pro.csproj", $@"{baseUrl}\AM.Net.{trimmedCompanyName}.{projectName}.Pro\AM.Net.{trimmedCompanyName}.{projectName}.Pro.csproj");

            //Datacollector
            Directory.Move($@"{baseUrl}\AM.Net.Company.Example.Dco", $@"{baseUrl}\AM.Net.{trimmedCompanyName}.{projectName}.Dco");
            File.Move($@"{baseUrl}\AM.Net.{trimmedCompanyName}.{projectName}.Dco\AM.Net.Company.Example.Dco.csproj", $@"{baseUrl}\AM.Net.{trimmedCompanyName}.{projectName}.Dco\AM.Net.{trimmedCompanyName}.{projectName}.Dco.csproj");

            //SLN
            File.Move($@"{baseUrl}\AM.Net.Company.Example.sln", $@"{baseUrl}\AM.Net.{trimmedCompanyName}.{projectName}.sln");

            if (options.Contains(ProjectSettingsEnum.GitRename))
            {

                string text = "";
                string computerUrl = "";

                if (options.Contains(ProjectSettingsEnum.GitCreateInputData))
                {
                    computerUrl = $@"{applicationPath}\Configuration\InputData\";
                    //Rename AM.Net.Company.Example.Data.csproj 
                    text = File.ReadAllText($@"{computerUrl}AM.Net.{trimmedCompanyName}.{projectNameInputdata}.Data/AM.Net.{trimmedCompanyName}.{projectNameInputdata}.Data.csproj");
                    text = text.Replace("AM.Net.Company.Example.Data", $"AM.Net.{trimmedCompanyName}.{projectNameInputdata}.Data");
                    text = text.Replace("ExampleInputData.cs", $"{projectNameInputdata}InputData.cs");
                    text = text.Replace("ExampleOutputData.cs", $"{projectNameInputdata}OutputData.cs");
                    File.WriteAllText($@"{computerUrl}AM.Net.{trimmedCompanyName}.{projectNameInputdata}.Data/AM.Net.{trimmedCompanyName}.{projectNameInputdata}.Data.csproj", text);

                    //Rename InputData.cs
                    text = File.ReadAllText($"{computerUrl}AM.Net.{trimmedCompanyName}.{projectNameInputdata}.Data/{projectNameInputdata}InputData.cs");
                    text = text.Replace("AM.Net.Company.Example.Data", $"AM.Net.{trimmedCompanyName}.{projectNameInputdata}.Data");
                    text = text.Replace("ExampleInputData", $"{projectNameInputdata}InputData");
                    File.WriteAllText($"{computerUrl}AM.Net.{trimmedCompanyName}.{projectNameInputdata}.Data/{projectNameInputdata}InputData.cs", text);
                    //Rename OutputData.cs
                    text = File.ReadAllText($"{computerUrl}AM.Net.{trimmedCompanyName}.{projectNameInputdata}.Data/{projectNameInputdata}OutputData.cs");
                    text = text.Replace("AM.Net.Company.Example.Data", $"AM.Net.{trimmedCompanyName}.{projectNameInputdata}.Data");
                    text = text.Replace("ExampleOutputData", $"{projectNameInputdata}OutputData");
                    File.WriteAllText($"{computerUrl}AM.Net.{trimmedCompanyName}.{projectNameInputdata}.Data/{projectNameInputdata}OutputData.cs", text);
                    //SvnManager.Commit(computerUrl, "GentleGit Action: Renamed ExampleData.csproj, InputData.cs, Outputdata.cs");
                    SetLabelsText(new BgwArguments { ActiveOption = ProjectSettingsEnum.GitCreateInputData });
                }


                //Rename
                //Trigger
                computerUrl = applicationPath + @"\Process\";

                text = File.ReadAllText(computerUrl + "AM.Net." + trimmedCompanyName + "." + projectNameInputdata + @".DCO.Trigger\AM.Net." + trimmedCompanyName + "." + projectNameInputdata + ".Dco.Trigger.csproj");
                text = text.Replace("AM.Net.Company.Example.Dco.Trigger",
                    $"AM.Net." + trimmedCompanyName + $".{projectNameInputdata}.Dco.Trigger");
                File.WriteAllText(computerUrl + "AM.Net." + trimmedCompanyName + "." + projectNameInputdata + @".DCO.Trigger\AM.Net." + trimmedCompanyName + "." + projectNameInputdata + ".Dco.Trigger.csproj", text);

                text = File.ReadAllText(computerUrl + "AM.Net." + trimmedCompanyName + "." + projectNameInputdata + @".DCO.Trigger\Program.cs");
                text = text.Replace("AM.Net.Company.Example.Dco.Trigger", $"AM.Net." + trimmedCompanyName + $".{projectNameInputdata}.Dco.Trigger");
                File.WriteAllText(computerUrl + "AM.Net." + trimmedCompanyName + "." + projectNameInputdata + @".DCO.Trigger\Program.cs", text);

                //DCO
                text = File.ReadAllText(computerUrl + "AM.Net." + trimmedCompanyName + "." + projectNameInputdata + @".Dco\AM.Net." + trimmedCompanyName + "." + projectNameInputdata + ".Dco.csproj");
                text = text.Replace("AM.Net.Company.Example.Dco",
                    $"AM.Net." + trimmedCompanyName + $".{projectNameInputdata}.Dco");
                if (options.Contains(ProjectSettingsEnum.SvnCreateInputData))
                {
                    text = text.Replace("AM.Net.Company.Example.Data", "AM.Net." + trimmedCompanyName + $".{projectNameInputdata}.Data");
                    text = text.Replace("ExampleInputData", $"{projectNameInputdata}InputData");
                }
                File.WriteAllText(computerUrl + "AM.Net." + trimmedCompanyName + "." + projectNameInputdata + @".Dco\AM.Net." + trimmedCompanyName + "." + projectNameInputdata + ".Dco.csproj", text);

                text = File.ReadAllText(computerUrl + "AM.Net." + trimmedCompanyName + "." + projectNameInputdata + @".Dco\Program.cs");
                text = text.Replace("AM.Net.Company.Example.Dco", $"AM.Net." + trimmedCompanyName + $".{projectNameInputdata}.Dco");
                File.WriteAllText(computerUrl + "AM.Net." + trimmedCompanyName + "." + projectNameInputdata + @".Dco\Program.cs", text);

                text = File.ReadAllText(computerUrl + "AM.Net." + trimmedCompanyName + "." + projectNameInputdata + @".Dco\MainScriptRunner\MainScriptRunner.cs");
                text = text.Replace("AM.Net.Company.Example.Dco", "AM.Net." + trimmedCompanyName + $".{projectNameInputdata}.Dco");
                File.WriteAllText(computerUrl + "AM.Net." + trimmedCompanyName + "." + projectNameInputdata + @".Dco\MainScriptRunner\MainScriptRunner.cs", text);

                text = File.ReadAllText(computerUrl + "AM.Net." + trimmedCompanyName + "." + projectNameInputdata + @".Dco\Scripts\ExampleTasksAnlegen.cs");
                text = text.Replace("AM.Net.Company.Example.Dco", "AM.Net." + trimmedCompanyName + $".{projectNameInputdata}.Dco");
                if (options.Contains(ProjectSettingsEnum.GitCreateInputData))
                {
                    text = text.Replace("AM.Net.Company.Example.Data", "AM.Net." + trimmedCompanyName + $".{projectNameInputdata}.Data");
                    text = text.Replace("ExampleInputData", $"{projectNameInputdata}InputData");
                }
                File.WriteAllText(computerUrl + "AM.Net." + trimmedCompanyName + "." + projectNameInputdata + @".Dco\Scripts\ExampleTasksAnlegen.cs", text);

                //Process
                text = File.ReadAllText(computerUrl + "AM.Net." + trimmedCompanyName + "." + projectNameInputdata + $@".Pro\AM.Net." + trimmedCompanyName + $".{projectNameInputdata}.Pro.csproj");
                text = text.Replace("AM.Net.Company.Example.Pro", "AM.Net." + trimmedCompanyName + $".{projectNameInputdata}.Pro");
                if (options.Contains(ProjectSettingsEnum.GitCreateInputData))
                {
                    text = text.Replace(@"AM.Net.Company.Example.Data", $@"AM.Net.{trimmedCompanyName}.{projectNameInputdata}.Data");
                }
                File.WriteAllText(computerUrl + "AM.Net." + trimmedCompanyName + "." + projectNameInputdata + $@".Pro\AM.Net." + trimmedCompanyName + $".{projectNameInputdata}.Pro.csproj", text);

                text = File.ReadAllText(computerUrl + "AM.Net." + trimmedCompanyName + "." + projectNameInputdata + @".Pro\Program.cs");
                text = text.Replace("AM.Net.Company.Example.Pro", "AM.Net." + trimmedCompanyName + $".{projectNameInputdata}.Pro");
                File.WriteAllText(computerUrl + "AM.Net." + trimmedCompanyName + "." + projectNameInputdata + @".Pro\Program.cs", text);

                text = File.ReadAllText(computerUrl + "AM.Net." + trimmedCompanyName + "." + projectNameInputdata + @".Pro\MainScriptRunner\MainScriptRunner.cs");
                text = text.Replace("AM.Net.Company.Example.Pro", "AM.Net." + trimmedCompanyName + $".{projectNameInputdata}.Pro");
                File.WriteAllText(computerUrl + "AM.Net." + trimmedCompanyName + "." + projectNameInputdata + @".Pro\MainScriptRunner\MainScriptRunner.cs", text);

                text = File.ReadAllText(computerUrl + "AM.Net." + trimmedCompanyName + "." + projectNameInputdata + @".Pro\DataEntry.cs");
                text = text.Replace("AM.Net.Company.Example.Pro", "AM.Net." + trimmedCompanyName + $".{projectNameInputdata}.Pro");
                if (options.Contains(ProjectSettingsEnum.GitCreateInputData))
                {
                    text = text.Replace("AM.Net.Company.Example.Data", "AM.Net." + trimmedCompanyName + $".{projectNameInputdata}.Data");
                    text = text.Replace("ExampleInputData", $"{projectNameInputdata}InputData");
                }
                File.WriteAllText(computerUrl + "AM.Net." + trimmedCompanyName + "." + projectNameInputdata + @".Pro\DataEntry.cs", text);

                text = File.ReadAllText(computerUrl + "AM.Net." + trimmedCompanyName + "." + projectNameInputdata + @".Pro\GlobalData.cs");
                text = text.Replace("AM.Net.Company.Example.Pro", "AM.Net." + trimmedCompanyName + $".{projectNameInputdata}.Pro");
                File.WriteAllText(computerUrl + "AM.Net." + trimmedCompanyName + "." + projectNameInputdata + @".Pro\GlobalData.cs", text);

                text = File.ReadAllText(computerUrl + "AM.Net." + trimmedCompanyName + "." + projectNameInputdata + @".Pro\Scripts\ProcessTicketExample.cs");
                text = text.Replace("AM.Net.Company.Example.Pro", "AM.Net." + trimmedCompanyName + $".{projectNameInputdata}.Pro");
                File.WriteAllText(computerUrl + "AM.Net." + trimmedCompanyName + "." + projectNameInputdata + @".Pro\Scripts\ProcessTicketExample.cs", text);

                //SLN
                text = File.ReadAllText(computerUrl + "AM.Net." + trimmedCompanyName + "." + projectNameInputdata + ".sln");
                text = text.Replace("AM.Net.Company.Example.Pro", "AM.Net." + trimmedCompanyName + $".{projectNameInputdata}.Pro");
                text = text.Replace("AM.Net.Company.Example.Dco", "AM.Net." + trimmedCompanyName + $".{projectNameInputdata}.Dco");
                text = text.Replace("AM.Net.Company.Example.Dco.Trigger", "AM.Net." + trimmedCompanyName + $".{projectNameInputdata}.Dco.Trigger");
                text = text.Replace("AM.Net.Company.Example.Data", $"AM.Net.{trimmedCompanyName}.{projectNameInputdata}.Data");
                text = text.Replace(@"AM.Net.Company.Example\", $@"AM.Net.{trimmedCompanyName}.{projectNameInputdata}\");
                text = text.Replace($@"\DTAG\Applications\AM.Net.{trimmedCompanyName}.{projectNameInputdata}\", $@"\{Helper.GetApplicationSnvFolderNameByCompany(companyName)}\Applications\AM.Net.Company.Example\");
                File.WriteAllText(computerUrl + "AM.Net." + trimmedCompanyName + "." + projectNameInputdata + ".sln", text);

                //Properties Pro
                string propertiesPro = $@"{computerUrl}\AM.Net.{trimmedCompanyName}.{projectNameInputdata}.Pro\Properties\";
                // Resources.Designer.cs
                string file = $"{propertiesPro}Resources.Designer.cs";
                text = File.ReadAllText(file);
                text = text.Replace("AM.Net.Company.Example.Pro", "AM.Net." + trimmedCompanyName + $".{projectNameInputdata}.Pro");
                File.WriteAllText(file, text);
                // Settings.Designer.cs
                file = $"{propertiesPro}Settings.Designer.cs";
                text = File.ReadAllText(file);
                text = text.Replace("AM.Net.Company.Example.Pro", "AM.Net." + trimmedCompanyName + $".{projectNameInputdata}.Pro");
                File.WriteAllText(file, text);

                //Properties DCO
                string propertiesDCO = $@"{computerUrl}\AM.Net.{trimmedCompanyName}.{projectNameInputdata}.Dco\Properties\";
                // Resources.Designer.cs
                file = $"{propertiesDCO}Resources.Designer.cs";
                text = File.ReadAllText(file);
                text = text.Replace("AM.Net.Company.Example.Dco", "AM.Net." + trimmedCompanyName + $".{projectNameInputdata}.Dco");
                File.WriteAllText(file, text);
                // Settings.Designer.cs
                file = $"{propertiesDCO}Settings.Designer.cs";
                text = File.ReadAllText(file);
                text = text.Replace("AM.Net.Company.Example.Dco", "AM.Net." + trimmedCompanyName + $".{projectNameInputdata}.Dco");
                File.WriteAllText(file, text);
                SetLabelsText(new BgwArguments { ActiveOption = ProjectSettingsEnum.GitRename });
                GitManager.CommitAndPush(computerUrl, Helper.GetGitBranchName(SelectedCompany, NewProjectName), "GentleGit Action: Renamed");
            }
        }

        /// <summary>
        /// Get all Projectsettings
        /// </summary>
        /// <returns></returns>
        private ProjectSettings GetProjectSettings()
        {
            ProjectSettings projectSettings = new ProjectSettings
            {
                CompanyName = SelectedCompany,
                FolderName = SelectedFolder,
                ProjectName = NewProjectNameEnabled ? NewProjectName : SelectedProject,
                SvnUrl = Config.GetSvnUrl(),
                GitUrl = Config.GetGitUrl(),
                ApplicationPath = GetApplicationPath(),
                Options = GetProjectOptions(),
                CreateNewProject = NewProjectNameEnabled
            };

            return projectSettings;
        }

        /// <summary>
        /// Get the selected Git and Svn Options
        /// </summary>
        /// <returns></returns>
        private List<ProjectSettingsEnum> GetProjectOptions()
        {
            List<ProjectSettingsEnum> options = new List<ProjectSettingsEnum>();

            //if (cbSvnConfiguration.IsChecked != null && cbSvnConfiguration.IsChecked.Value)
            //{
            //    options.Add(ProjectSettingsEnum.SvnConfiguration);
            //}

            if (SvnProcess)
            {
                options.Add(ProjectSettingsEnum.SvnProcess);
            }

            if (SvnCommon)
            {
                options.Add(ProjectSettingsEnum.SvnCommon);
            }

            if (SvnApplications)
            {
                options.Add(ProjectSettingsEnum.SvnApplications);
            }

            if (NewProjectNameEnabled)
            {
                if (GitCreateInputdata)
                {
                    options.Add(ProjectSettingsEnum.GitCreateInputData);
                }

                if (GitRename)
                {
                    options.Add(ProjectSettingsEnum.GitRename);
                }
            }

            if (GitCommon)
            {
                options.Add(ProjectSettingsEnum.GitCommon);
            }

            if (GitApplications)
            {
                options.Add(ProjectSettingsEnum.GitApplications);
            }

            if (GitConfiguration)
            {
                options.Add(ProjectSettingsEnum.GitConfiguration);
            }

            if (GitProcess)
            {
                options.Add(ProjectSettingsEnum.GitProcess);
            }

            return options;
        }

        /// <summary>
        /// Get the Application Path with destination, company and project name. Add Prod to Project name if not there
        /// </summary>
        /// <returns></returns>
        private string GetApplicationPath()
        {
            string projectname = NewProjectNameEnabled ? NewProjectName : SelectedProject;
            if (!projectname.StartsWith("Prod"))
            {
                projectname = "Prod" + projectname;
            }
            return $@"{Destination}{SelectedCompany}\{projectname}";
        }

        /// <summary>
        /// Set the Action utton text  wit the remaining tasks. If runningThreads = 0 then rename the file and restore nugetpackages
        /// </summary>
        private void UpdateUI()
        {
            System.Windows.Application.Current.Dispatcher.Invoke((MethodInvoker)delegate
            {
                ButtonText = _runningThreadCounter > 0 ? "Remaining tasks: " + _runningThreadCounter : "Checking out project";

                if (_runningThreadCounter == 0)
                {
                    ButtonEnabled = true;
                    StatusVisible = false;
                    ProjectSettings setting = GetProjectSettings();
                    if (setting.CreateNewProject)
                    {
                        CheckIfReadyToRenameFiles(setting);
                    }
                    RestoreNugetPackages(setting);
                    System.Windows.MessageBox.Show(Constants.Done);
                }
            });
        }

        private ICommand _createNewProjectCommand;

        public ICommand CreateNewProjectCommand
        {
            get
            {
                if (_createNewProjectCommand == null)
                {
                    _createNewProjectCommand = new RelayCommand(
                        param => this.NewProject_Click(),
                        param => true
                    );
                }
                return _createNewProjectCommand;
            }
        }

        private ICommand _checkOutCommand;

        public ICommand CheckOutCommand
        {
            get
            {
                if (_checkOutCommand == null)
                {
                    _checkOutCommand = new RelayCommand(
                        param => this.CheckoutProject_Click(),
                        param => true
                    );
                }
                return _checkOutCommand;
            }
        }

        /// <summary>
        /// executed if Create New Project Radio button is checked
        /// </summary>
        private void NewProject_Click()
        {
            ButtonText = "Create new Project";
            NewProjectNameEnabled = true;

            GitCommonEnabled = true;
            GitApplicationsEnabled = true;
            GitRenameEnabled = true;
            GitApplications = true;
            GitConfiguration = true;
            GitCreateInputdata = true;
            GitProcess = true;
            GitCommon = true;
            GitRename = true;

            SvnApplications = false;
            SvnCommon = false;
            SvnProcess = false;
            //cbSvnConfiguration.IsChecked = false;

            GitOptionsSpan = 2;

            ToggleControlsStatus(isEnabled: true);
        }

        /// <summary>
        /// executed if CheckOut Radio button is checked
        /// </summary>
        private void CheckoutProject_Click()
        {
            ButtonText = "Checkout Project";
            GitCreateInputdata = false;
            GitRename = false;
            GitOptionsSpan = 1;

            if (!isConnected)
            {
                ToggleControlsStatus(isEnabled: false);
            }
            else
            {
                ToggleControlsStatus(true);
            }
        }


        private ICommand _actionButtonCommand;

        public ICommand ActionButtonCommand
        {
            get
            {
                if (_actionButtonCommand == null)
                {
                    _actionButtonCommand = new RelayCommand(
                        param => this.StartAction(),
                        param => this.CanStartAction()
                    );
                }
                return _actionButtonCommand;
            }
        }

        /// <summary>
        /// Check if Destination, Company and Project is filled and min one Option is checked.
        /// </summary>
        /// <returns>true if all filled</returns>
        private bool CanStartAction()
        {
            // Verify command can be executed here
            if (!string.IsNullOrWhiteSpace(Destination) && !string.IsNullOrEmpty(SelectedCompany))
            {
                if (!string.IsNullOrEmpty(Config.GetGitPassword()) &&
                    !string.IsNullOrEmpty(Config.GetGitUsername()))
                {
                    if ((CheckOutRB && !string.IsNullOrEmpty(SelectedProject)) || //If Checkout then SelectedProject needs to be filled
                        (CreateNewProjectRB && (!string.IsNullOrEmpty(NewProjectName)))) //If CreateNewProject then NewProjectName needs to be filled
                    {
                        if (GetProjectOptions().Count > 0) // min 1 Option 
                        {
                            return true;
                        }
                       
                    }
                   
                }
                else
                {
                    //System.Windows.MessageBox.Show(
                    //    "Git Username or Git Password is empty. Please enter in Settings.");
                }
            }
            else
            {
                //System.Windows.MessageBox.Show("Destination path or projectname or Company is empty!");

            }

            return false;
        }

        /// <summary>
        /// executed if Action button pressed, Checkout or CreateNewProject started
        /// </summary>
        private void StartAction()
        {
            // Save command execution logic
            ProjectSettings projectSettings = GetProjectSettings();

            if (CreateNewProjectRB)
            {
                CreateNewProject(projectSettings);
            }
            else
            {
                CheckOutProject(projectSettings);
            }
        }

        private ICommand _chooseFolderCommand;

        public ICommand ChooseFolderCommand
        {
            get
            {
                if (_chooseFolderCommand == null)
                {
                    _chooseFolderCommand = new RelayCommand(
                        param => this.ChooseFolderAction(),
                        param => true
                    );
                }
                return _chooseFolderCommand;
            }
        }


        /// <summary>
        /// Open a dialog to choose a folder where to save the project
        /// </summary>
        private void ChooseFolderAction()
        {
            using (FolderBrowserDialog dialog = new FolderBrowserDialog())
            {
                dialog.Description = "Choose destination folder";
                dialog.ShowNewFolderButton = true;

                DialogResult result = dialog.ShowDialog();

                if (result == System.Windows.Forms.DialogResult.OK)
                {
                    Destination = dialog.SelectedPath;
                }
            }
        }

        private ICommand _companySelectionChangedCommand;
      

        public ICommand CompanySelectionChangedCommand
        {
            get
            {
                if (_companySelectionChangedCommand == null)
                {
                    _companySelectionChangedCommand = new RelayCommand(
                        param => this.CompanySelectionChanged(),
                        param => true
                    );
                }
                return _companySelectionChangedCommand;
            }
        }

        private ICommand _folderSelectionChangedCommand;

        public ICommand FolderSelectionChangedCommand
        {
            get
            {
                if (_folderSelectionChangedCommand == null)
                {
                    _folderSelectionChangedCommand = new RelayCommand(
                        param => this.FoldersSelectionChanged(),
                        param => this.CanFolderChanged()
                    );
                }
                return _folderSelectionChangedCommand;
            }
        }

        /// <summary>
        /// Check if Selected Company is filled
        /// </summary>
        /// <returns>true if Company is filled</returns>
        public bool CanFolderChanged()
        {
            if (string.IsNullOrEmpty(SelectedCompany))
            {
                return false;
            }

            return true;
        }

        /// <summary>
        /// Check if Project and Folder is filled
        /// </summary>
        /// <returns>true if Project and Folder is filled</returns>
        public bool CanProjectsChanged()
        {
            if (string.IsNullOrEmpty(SelectedCompany) ||string.IsNullOrEmpty(SelectedFolder))
            {
                return false;
            }

            return true;
        }

        private ICommand _projectSelectionChangedCommand;

        public ICommand ProjectSelectionChangedCommand
        {
            get
            {
                if (_projectSelectionChangedCommand == null)
                {
                    _projectSelectionChangedCommand = new RelayCommand(
                        param => this.ProjectsSelectionChanged(),
                        param => this.CanProjectsChanged()
                    );
                }
                return _projectSelectionChangedCommand;
            }
        }

        /// <summary>
        /// Get the repositories in the selected Company section from SVN
        /// Check if they are valid to display and select the first element
        /// </summary>
        private void CompanySelectionChanged()
        {
            string svnQuery = $"{Config.GetSvnUrl()}/{SelectedCompany}";

            if (!string.IsNullOrWhiteSpace(svnQuery))
            {
                StatusVisible = true;

                var svnBranches = new List<String>() { "Git Projects" };
                svnBranches.AddRange(SvnManager.GetBranches(svnQuery));
                

                if (svnBranches != null && svnBranches.Any())
                {
                    FolderNames = svnBranches.Where(Helper.IsValidFolder).ToList();
                    SelectedFolderIndex = 0;
                }

                StatusVisible = false;
                AddGitProcessBranchesToList();
            }
            else
            {
                System.Windows.MessageBox.Show("SvnUrl is empty!");
            }
        }

        /// <summary>
        /// Get the companies repository and show all project repositories from Git and SVN in there
        /// Select the first project as default value
        /// </summary>
        private void FoldersSelectionChanged()
        {
            if (!string.IsNullOrEmpty(SelectedFolder))
            {
                if (SelectedFolder.Equals("Git Projects"))
                {
                    AddGitProcessBranchesToList();
                }
                else
                {
                    string svnQuery = $"{Config.GetSvnUrl()}/{SelectedCompany}/{SelectedFolder}";

                    if (!string.IsNullOrWhiteSpace(svnQuery))
                    {
                        StatusVisible = true;


                        string[] svnBranches = SvnManager.GetBranches(svnQuery);


                        if (svnBranches != null && svnBranches.Any())
                        {
                            CheckOutProjects = svnBranches.Where(i => !string.IsNullOrWhiteSpace(i)).ToList();
                            SelectedProjectIndex = 0;
                        }

                        StatusVisible = false;

                    }
                    else
                    {
                        System.Windows.MessageBox.Show("SvnUrl is empty!");
                    }
                }
            }
           

        }

        /// <summary>
        /// Get all branches from git repos
        /// If it is not a project to be created check if the project is inside the branches we got earlier
        /// If it is inside the branchesarray, deactivate the checkbox for SVN
        /// </summary>
        private void ProjectsSelectionChanged()
        {
            string gitUrl = Config.GetGitUrl();
            var CompanyName = SelectedCompany;
            var ProjectName = SelectedProject;
            if (!String.IsNullOrEmpty(CompanyName) && !String.IsNullOrEmpty(ProjectName))
            {
                string gitBranchName = Helper.GetGitBranchName(CompanyName, ProjectName);
                string[] gitBranchesCommon = GitManager.GetBranches(gitUrl + ".Common");
                string[] gitBranchesProcesses =
                    GitManager.GetBranches(gitUrl + Helper.GetProcessesGitRepoNameByCompany(CompanyName));
                string[] gitBranchesApplications =
                    GitManager.GetBranches(gitUrl + Helper.GetApplicationGitRepoNameByCompany(CompanyName));
                //string[] gitBranchesConfiguration = GitManager.GetBranches(gitUrl + Helper.GetConfigurationGitRepoNameByCompany(CompanyName));

                Boolean isBranchOnGitCommon = Helper.IsProjectOnGit(gitBranchName, gitBranchesCommon);
                Boolean isBranchOnGitProcesses = Helper.IsProjectOnGit(gitBranchName, gitBranchesProcesses);
                Boolean isBranchOnGitApplications = Helper.IsProjectOnGit(gitBranchName, gitBranchesApplications);

                if (CheckOutRB)
                {
                    SvnCommonEnabled= !isBranchOnGitCommon;
                    SvnCommon = !isBranchOnGitCommon;
                    GitCommonEnabled = isBranchOnGitCommon;
                    GitCommon = isBranchOnGitCommon;
                    SvnApplicationsEnabled = !isBranchOnGitApplications;
                    SvnApplications = !isBranchOnGitApplications;
                    GitApplicationsEnabled = isBranchOnGitApplications;
                    GitApplications = isBranchOnGitApplications;
                    SvnProcessEnabled = !isBranchOnGitProcesses;
                    SvnProcess = !isBranchOnGitProcesses;
                    GitProcessEnabled = isBranchOnGitProcesses;
                    GitProcess = isBranchOnGitProcesses;
                    //cbSvnConfiguration.IsEnabled = !Helper.IsProjectOnGit(ddlSvnProjects?.SelectedValue?.ToString(), gitBranchesConfiguration);

                }
            }
        }

        #region BackgroundWorker events

        /// <summary>
        /// Restore the nuget packages from SLN
        /// </summary>
        /// <param name="setting"></param>
        private void RestoreNugetPackages(ProjectSettings setting)
        {
            //1st Step: Write nuget.exe to HDD
            string nuget = Path.Combine(Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location), @"Nuget\nuget.exe");
            //2nd Step: Identify SLN


            var processPath = $@"{setting.ApplicationPath}\Process";
            var slnName = new DirectoryInfo(processPath).GetFiles().FirstOrDefault(x => x.Extension == ".sln")?.FullName;
            var outputDir = $@"{processPath}\packages";
            //3rd Step: Run Nuget
            var proc = new ProcessStartInfo
            {
                FileName = nuget,
                Arguments = $"restore \"{slnName}\" -OutputDirectory \"{outputDir}\""
            };

            var procInfo = System.Diagnostics.Process.Start(proc);
            procInfo?.WaitForExit();
        }

        /// <summary>
        /// Set the Label to finished if Bgw is finished
        /// </summary>
        /// <param name="arguments"></param>
        private void SetLabelsText(BgwArguments arguments)
        {
            ProjectSettingsEnum activeOption = arguments.ActiveOption;

            switch (activeOption)
            {
                case ProjectSettingsEnum.SvnCommon:
                    LabelSvnCommon = Constants.Finished;
                    break;
                case ProjectSettingsEnum.SvnApplications:
                    LabelSvnApplications= Constants.Finished;
                    break;
                case ProjectSettingsEnum.SvnProcess:
                    LabelSvnProcess = Constants.Finished;
                    break;
                //case ProjectSettingsEnum.SvnConfiguration:
                //    lblSvnConfiguration.Content = Constants.Finished;
                //    break;
                case ProjectSettingsEnum.GitCommon:
                        LabelGitCommon = Constants.Finished;
                    break;
                case ProjectSettingsEnum.GitApplications:
                    LabelGitApplications = Constants.Finished;
                    break;
                case ProjectSettingsEnum.GitProcess:
                    LabelGitProcess = Constants.Finished;
                    break;
                case ProjectSettingsEnum.GitConfiguration:
                    LabelGitConfiguration = Constants.Finished;
                    break;
                case ProjectSettingsEnum.GitCreateInputData:
                    LabelGitCreateInputdata = Constants.Finished;
                    break;
                case ProjectSettingsEnum.GitRename:
                    LabelGitRename = Constants.Finished;
                    break;
            }
        }

        /// <summary>
        /// Enable or Disable the Buttons
        /// </summary>
        /// <param name="isEnabled"></param>
        private void ToggleControlsStatus(bool isEnabled)
        {
            //CreateNewProjectRB = isEnabled;
            //CheckOutRB = isEnabled;

            DdlCompanysEnabled = isEnabled;
            DdlFoldersEnabled = isEnabled;
            DdlProjectsEnabled = isEnabled;

            DestinationEnabled = isEnabled;
            ChooseFolderEnabled = isEnabled;

            SvnApplicationsEnabled = isEnabled;
            SvnCommonEnabled = isEnabled;
            //cbSvnConfiguration.IsEnabled = isEnabled;
            SvnProcessEnabled = isEnabled;

            GitApplicationsEnabled = isEnabled;
            GitCommonEnabled = isEnabled;
            GitConfigurationEnabled = isEnabled;
            GitProcessEnabled = isEnabled;

           ButtonEnabled = isEnabled;
        }

        /// <summary>
        /// Get the precent Value from processedObject of totalObjects
        /// </summary>
        /// <param name="processedObjects"></param>
        /// <param name="totalObjects"></param>
        /// <returns></returns>
        private string GetLabelPercentageText(long processedObjects, long totalObjects)
        {
            return $"{(int)Math.Round((double)(100 * processedObjects) / totalObjects)} %";
        }

        /// <summary>
        ///  Get the precent Value from currentObjects of totalObjects
        /// </summary>
        /// <param name="current"></param>
        /// <param name="total"></param>
        /// <param name="bytes"></param>
        /// <returns>Label string</returns>
        private string GetLabelPercentageText(int current, int total, long bytes)
        {
            string percent = $"{(int)Math.Round((double)(100 * current) / total)} %";
            return "Check Out done. Push " + percent;
        }

        /// <summary>
        /// Get processed Nuggets for Label
        /// </summary>
        /// <param name="processedObjects"></param>
        /// <returns>Label string</returns>
        private string GetLabelNuggetText(long processedObjects)
        {
            return $"{Math.Round(processedObjects / 1024.0 / 1024.0 * 4, 2)} processed Nuggets ";
        }


        private void SvnApplicationsController_ProgressEvent(SvnProgressEventArgs progressEventArgs)
        {
            System.Windows.Application.Current.Dispatcher.Invoke((MethodInvoker)delegate
            {
                LabelSvnApplications = GetLabelNuggetText(progressEventArgs.Progress);
            });
        }

        private void SvnCommonController_ProgressEvent(SvnProgressEventArgs progressEventArgs)
        {
            System.Windows.Application.Current.Dispatcher.Invoke((MethodInvoker)delegate
            {
                LabelSvnCommon= GetLabelNuggetText(progressEventArgs.Progress);
            });
        }

        private void SvnProcessController_ProgressEvent(SvnProgressEventArgs progressEventArgs)
        {
            System.Windows.Application.Current.Dispatcher.Invoke((MethodInvoker)delegate
            {
                LabelSvnCommon = GetLabelNuggetText(progressEventArgs.Progress);
            });
        }

        private void GitApplicationController_ProgressEvent(TransferProgress progress)
        {
            System.Windows.Application.Current.Dispatcher.Invoke((MethodInvoker)delegate
            {
                LabelGitApplications = GetLabelPercentageText(progress.ReceivedObjects, progress.TotalObjects);
            });
        }

        private void GitApplicationController_PushProgressEvent(int current, int total, long bytes)
        {
            System.Windows.Application.Current.Dispatcher.Invoke((MethodInvoker)delegate
            {
               LabelGitApplications= GetLabelPercentageText(current, total, bytes);
            });
        }

        private void GitCommonController_ProgressEvent(TransferProgress progress)
        {
            System.Windows.Application.Current.Dispatcher.Invoke((MethodInvoker)delegate
            {
                LabelGitCommon = GetLabelPercentageText(progress.ReceivedObjects, progress.TotalObjects);
            });
        }

        private void GitCommonController_PushProgressEvent(int current, int total, long bytes)
        {
            System.Windows.Application.Current.Dispatcher.Invoke((MethodInvoker)delegate
            {
                LabelGitCommon = GetLabelPercentageText(current, total, bytes);
            });
        }

        private void GitConfigurationController_ProgressEvent(TransferProgress progress)
        {
            System.Windows.Application.Current.Dispatcher.Invoke((MethodInvoker)delegate
            {
               LabelGitConfiguration = GetLabelPercentageText(progress.ReceivedObjects, progress.TotalObjects);
            });
        }

        private void GitConfigurationController_PushProgressEvent(int current, int total, long bytes)
        {
            System.Windows.Application.Current.Dispatcher.Invoke((MethodInvoker)delegate
            {
                LabelGitConfiguration = GetLabelPercentageText(current, total, bytes);
            });
        }

        private void GitProcessController_ProgressEvent(TransferProgress progress)
        {
            System.Windows.Application.Current.Dispatcher.Invoke((MethodInvoker)delegate
            {
                LabelGitProcess = GetLabelPercentageText(progress.ReceivedObjects, progress.TotalObjects);
            });
        }

        private void GitProcessController_PushProgressEvent(int current, int total, long bytes)
        {
            System.Windows.Application.Current.Dispatcher.Invoke((MethodInvoker)delegate
            {
                LabelGitProcess = GetLabelPercentageText(current, total, bytes);
            });
        }
        /// <summary>
        /// Start the CheckoutProcess and calc the runningThreadCounter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void BgWorker_DoWork(object sender, DoWorkEventArgs e)
        {
            _runningThreadCounter++;

            BgwArguments arguments = e.Argument as BgwArguments;

            try
            {
                if (arguments != null)
                {
                    StartCheckOutProcess(arguments);
                }
            }
            catch (Exception ex)
            {
                System.Windows.Application.Current.Dispatcher.Invoke((MethodInvoker)delegate
                {
                    System.Windows.MessageBox.Show(ex.ToString());
                });
            }
            finally
            {
                _runningThreadCounter--;

                System.Windows.Application.Current.Dispatcher.Invoke((MethodInvoker)delegate
                {

                    SetLabelsText(arguments);
                });
            }

            UpdateUI();
        }

        #endregion
    }
}