﻿using AM.Tools.GentleGit.Business.Model;
using AM.Tools.GentleGit.Common;
using AM.Tools.GentleGit.Common.Enum;
using SharpSvn;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.IO;
using System.Linq;

namespace AM.Tools.GentleGit.Business
{
    public class SvnManager
    {
        public delegate void ProgressHandler(SvnProgressEventArgs progressEventArgs);
        public event ProgressHandler ProgressEvent;

        #region Ctor

        public SvnManager()
        {
            //do nothing
        }

        #endregion

        #region Public methods

        /// <summary>
        /// Get all branches into one collection and insert them into a list to display them
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        public static string[] GetBranches(string query)
        {
            List<string> branches = null;

            if (!string.IsNullOrWhiteSpace(query))
            {
                Collection<SvnListEventArgs> contents = GetBranchesInternal(query);

                if (contents != null && contents.Any())
                {
                    branches = new List<string>();
                  
                    branches.AddRange(contents.Select(entry => entry.Path));
                }
            }
            else
            {
                LogHelper.Instance.Log("Svn query is null or empty.");
            }

            return branches?.ToArray();
        }

        /// <summary>
        /// Create a local directory and checkout the repository into this directroy
        /// If it fails to checkout, delete the repository
        /// </summary>
        /// <param name="url"></param>
        /// <param name="destination"></param>
        public bool CheckOut(string url, string destination)
        {
            if (!string.IsNullOrWhiteSpace(url) && !string.IsNullOrWhiteSpace(destination))
            {
                using (SvnClient client = new SvnClient())
                {
                    try
                    {
                        Directory.CreateDirectory(destination);

                        client.Progress += Client_Progress;
                        return client.CheckOut(new Uri(url), destination);
                    }
                    catch (SvnRepositoryIOException)
                    {
                        Directory.Delete(destination);

                        throw new Exception("Folder doesn't exist on Server!");
                    }
                    catch (Exception ex)
                    {
                        throw new Exception(ex.ToString().Contains("run 'cleanup'") ? "Please make a Cleanup first. " : ex.ToString());
                    }
                }
            }
            else
            {
                LogHelper.Instance.Log($"Url or destination is null. SvnUrl='{url ?? "NULL"}'; Destination='{destination ?? "NULL"}'");
            }

            return false;
        }

        public static void Commit(string path, string logMessage = "Commit")
        {
            using (SvnClient client = new SvnClient())
            {
                SvnCommitArgs commit = new SvnCommitArgs { LogMessage = logMessage };

                client.Commit(path, commit);
            }
        }

        public static bool CreateNewProject(string svnUrl, ProjectSettings projectSettings)
        {
            if (!string.IsNullOrWhiteSpace(svnUrl) && projectSettings != null)
            {
                try
                {
                    string companyName = projectSettings.CompanyName;
                    string folderName = projectSettings.FolderName;
                    string projectName = projectSettings.ProjectName;

                    string baseUrl = $"{svnUrl}/{companyName}/{folderName}/{projectName}"; 

                    if (projectSettings.Options.Contains(ProjectSettingsEnum.SvnProcess))
                    {
                        Copy($"{svnUrl}/Deutsche Telekom/branches/Dev", baseUrl);
                        MoveProcess(projectName, companyName, baseUrl);
                    }

                    if (projectSettings.Options.Contains(ProjectSettingsEnum.SvnCommon))
                    {
                        Copy($"{svnUrl}/Common/branches/Dev", $"{svnUrl}/Common/{folderName}/{projectName}");
                    }

                    if (projectSettings.Options.Contains(ProjectSettingsEnum.SvnApplications))
                    {
                        string applicationFolder = Helper.GetApplicationSnvFolderNameByCompany(companyName);
                        Copy($"{svnUrl}/{applicationFolder}/branches/Dev", $"{svnUrl}/{applicationFolder}/{folderName}/{projectName}");
                    }


                    if (projectSettings.Options.Contains(ProjectSettingsEnum.SvnCreateInputData))
                    {
                        String projectNameInputdata = projectName.Substring(4); //Without 'Prod'
                        string trimmedCompanyName = companyName.Replace(" ", "");
                        Copy(svnUrl + "/Deutsche Telekom/trunk/Configuration/InputData/AM.Net.Company.Example.Data"
                            , svnUrl + "/" + companyName + $"/trunk/Configuration/InputData/AM.Net.{trimmedCompanyName}.{projectNameInputdata}.Data");


                        if (projectSettings.Options.Contains(ProjectSettingsEnum.SvnRename))
                        {
                            Move(svnUrl + "/" + companyName +
                                 $"/trunk/Configuration/InputData/AM.Net.{trimmedCompanyName}.{projectNameInputdata}.Data/AM.Net.Company.Example.Data.csproj"
                                , svnUrl + "/" + companyName + $"/trunk/Configuration/InputData/AM.Net.{trimmedCompanyName}.{projectNameInputdata}.Data/AM.Net.{trimmedCompanyName}.{projectNameInputdata}.Data.csproj");

                            Move(svnUrl + "/" + companyName +
                                 $"/trunk/Configuration/InputData/AM.Net.{trimmedCompanyName}.{projectNameInputdata}.Data/ExampleInputData.cs"
                                , svnUrl + "/" + companyName + $"/trunk/Configuration/InputData/AM.Net.{trimmedCompanyName}.{projectNameInputdata}.Data/{projectNameInputdata}InputData.cs");

                            Move(svnUrl + "/" + companyName +
                                 $"/trunk/Configuration/InputData/AM.Net.{trimmedCompanyName}.{projectNameInputdata}.Data/ExampleOutputData.cs"
                                , svnUrl + "/" + companyName + $"/trunk/Configuration/InputData/AM.Net.{trimmedCompanyName}.{projectNameInputdata}.Data/{projectNameInputdata}OutputData.cs");
                        }
                    }
          
                    //Commit(baseUrl, "Init");

                    return true;
                }
                catch (Exception ex)
                {
                    LogHelper.Instance.Log("Error while creating project", ex);
                }
            }
            else
            {
                LogHelper.Instance.Log($"SvnUrl or projectSettings is null. SvnUrl='{svnUrl ?? "NULL"}'; ProjectSettings={(projectSettings != null ? "OK" : "NULL")}");
            }

            return false;
        }

        #endregion

        #region Private methods

        private static void MoveProcess(string projectName, string companyName, string baseUrl)
        {
            string trimedCompanyName = companyName.Replace(" ", "");

            //Trigger
            Move($"{baseUrl}/AM.Net.Company.Example.Dco.Trigger", $"{baseUrl}/AM.Net.{trimedCompanyName}.{projectName}.Dco.Trigger");
            Move($"{baseUrl}/AM.Net.{trimedCompanyName}.{projectName}.Dco.Trigger/AM.Net.Company.Example.Dco.Trigger.csproj", $"{baseUrl}/AM.Net.{trimedCompanyName}.{projectName}.Dco.Trigger/AM.Net.{trimedCompanyName}.{projectName}.Dco.Trigger.csproj");

            //Process
            Move($"{baseUrl}/AM.Net.Company.Example.Pro", $"{baseUrl}/AM.Net.{trimedCompanyName}.{projectName}.Pro");
            Move($"{baseUrl}/AM.Net.{trimedCompanyName}.{projectName}.Pro/AM.Net.Company.Example.Pro.csproj", $"{baseUrl}/AM.Net.{trimedCompanyName}.{projectName}.Pro/AM.Net.{trimedCompanyName}.{projectName}.Pro.csproj");

            //Datacollector
            Move($"{baseUrl}/AM.Net.Company.Example.Dco", $"{baseUrl}/AM.Net.{trimedCompanyName}.{projectName}.Dco");
            Move($"{baseUrl}/AM.Net.{trimedCompanyName}.{projectName}.Dco/AM.Net.Company.Example.Dco.csproj", $"{baseUrl}/AM.Net.{trimedCompanyName}.{projectName}.Dco/AM.Net.{trimedCompanyName}.{projectName}.Dco.csproj");

            //SLN
            Move($"{baseUrl}/AM.Net.Company.Example.sln", $"{baseUrl}/AM.Net.{trimedCompanyName}.{projectName}.sln");
        }

        /// <summary>
        /// Get all repositories in this passed Query/Folder/Section
        /// </summary>
        /// <param name="query"></param>
        /// <returns></returns>
        private static Collection<SvnListEventArgs> GetBranchesInternal(string query)
        {
            Collection<SvnListEventArgs> contents = null;

            try
            {
                using (SvnClient svnClient = new SvnClient())
                {

                    SvnListArgs args = new SvnListArgs();

                    args.Notify += Args_Notify;
                    args.Progress += Args_Progress;

                    svnClient.GetList(new Uri(query), args, out contents);
                }
            }
            catch (Exception ex)
            {
                LogHelper.Instance.Log($@"Error while fetching branches, URL: {query}", ex);
            }

            return contents;
        }

        private static void Args_Progress(object sender, SvnProgressEventArgs e)
        {
            var progress = e.Progress;
            var totalProgress = e.TotalProgress;
        }

        private static void Args_Notify(object sender, SvnNotifyEventArgs e)
        {
            var test = e;
        }

        private static void Move(string fromPath, string toPath)
        {
            using (SvnClient client = new SvnClient())
            {
                SvnMoveArgs move = new SvnMoveArgs { LogMessage = "GentleGit action: Move" };
                client.RemoteMove(new Uri(fromPath), new Uri(toPath), move);
            }
        }

        private static void Copy(string urlFrom, string urlTo)
        {
            using (SvnClient client = new SvnClient())
            {
                SvnCopyArgs copy = new SvnCopyArgs { LogMessage = "GentleGit action: Creating new branch" };
                client.RemoteCopy(urlFrom, new Uri(urlTo), copy);
            }
        }

        private void Client_Progress(object sender, SvnProgressEventArgs e)
        {
            ProgressEvent?.Invoke(e);
        }

        #endregion
    }
}
