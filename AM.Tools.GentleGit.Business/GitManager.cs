﻿using AM.Tools.GentleGit.Business.Model;
using AM.Tools.GentleGit.Common;
using AM.Tools.GentleGit.Common.Enum;
using LibGit2Sharp;
using LibGit2Sharp.Handlers;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows;
using System.Windows.Documents;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;

namespace AM.Tools.GentleGit.Business
{
    public class GitManager
    {
        public delegate void ProgressHandler(TransferProgress progress);
        public delegate void PushProgressHandler(int current, int total, long bytes);
        public event PushProgressHandler PushProgressEvent;
        public event ProgressHandler ProgressEvent;

        #region Ctor

        public GitManager()
        {
            //do nothing
        }

        #endregion

        #region Public methods

        /// <summary>
        /// If Gitcommon is checked then checkout the Dev common branch and create a new branch with the projectname and the checkedout content
        /// </summary>
        /// <param name="gitUrl"></param>
        /// <param name="projectSettings"></param>
        /// <returns>isSuccess</returns>
        public bool CreateNewProject(string gitUrl, string destination, string projectName)
        {
            bool isSuccess = false;

            if (!string.IsNullOrWhiteSpace(gitUrl) && !string.IsNullOrWhiteSpace(destination) && !string.IsNullOrWhiteSpace(projectName))
            {
                try
                {
                    isSuccess = CheckOut(gitUrl, $@"{destination}", "master");

                    CreateNewRemoteBranch($@"{destination}", projectName);
                }
                catch (Exception ex)
                {
                    LogHelper.Instance.Log($"Error while creating project at Git URL {gitUrl}", ex);
                }
            }
            else
            {
                LogHelper.Instance.Log($"GitUrl ({gitUrl}) or destination ({destination}) or projectName ({projectName}) is empty ");
            }

            return isSuccess;
        }

        /// <summary>
        /// Get the username and password for git
        /// Get all branches and search for the one we want to check out
        /// </summary>
        /// <param name="gitUrl"></param>
        /// <param name="destination"></param>
        /// <param name="projectName"></param>
        /// <returns>isSuccess</returns>
        public bool CheckOut(string gitUrl, string destination, string projectName)
        {
            if (Helper.IsDirectoryEmpty(destination))
            {
                CloneOptions options = GetCloneOptions();

                if (options != null)
                {
                    if (IsProjectBranchInRepo(gitUrl, projectName, options, out var targetBranchName))
                        if (!string.IsNullOrEmpty(targetBranchName))
                        {
                            options.BranchName = targetBranchName;
                            options.OnTransferProgress = TransferProgress;

                            Repository.Clone(gitUrl, destination, options);
                            return true;
                        }
                }
                else
                {
                    LogHelper.Instance.Log("CloneOptions is null, check username and password in App.config.");
                }
            }
            else
            {
                LogHelper.Instance.Log("Git destination folder is not empty.");
            }

            return false;
        }

        public static bool IsProjectBranchInRepo(string gitUrl, string projectName, CloneOptions options, out string targetBranchName)
        {
            if (options == null)
            {
                options = GetCloneOptions();
            }

            if (options != null)
            {
                IEnumerable<string> branches = GetBranchesInternal(gitUrl, options);

                if (branches != null && branches.Any())
                {
                    string targetBranchName2 = branches.FirstOrDefault(i => i.Contains(projectName));

                    if (!string.IsNullOrEmpty(targetBranchName2))
                    {
                        targetBranchName = targetBranchName2;
                        return true;
                    }
                    else
                    {
                        LogHelper.Instance.Log($"GIT repository with URL='{gitUrl}' has no branch containing project with name '{projectName}'.");
                    }
                }
                else
                {
                    LogHelper.Instance.Log($"GIT repository with URL='{gitUrl}' branches are null or empty.");
                }
            }
            
            targetBranchName = "";
            return false;
        }

        /// <summary>
        /// Get the credentials to the gitlogin and receive all branches in the passed url
        /// return them as an array
        /// </summary>
        /// <param name="gitUrl"></param>
        /// <returns></returns>
        public static string[] GetBranches(string gitUrl)
        {
            IEnumerable<string> branches = null;
            if (!string.IsNullOrWhiteSpace(gitUrl))
            {
                CloneOptions options = GetCloneOptions();

                if (options != null)
                {
                    branches = GetBranchesInternal(gitUrl, options);

                    if (branches == null || !branches.Any())
                    {
                        LogHelper.Instance.Log($@"BranchCollection for repository={gitUrl} is null or empty.");
                    }
                }
                else
                {
                    LogHelper.Instance.Log("CloneOptions is null, check username and password in App.config.");
                }
            }
            else
            {
                LogHelper.Instance.Log("GitUrl is null or empty.");
            }

            return branches?.ToArray();
        }

        #endregion

        #region Private methods        

        /// <summary>
        /// create a repository with the local path to the project
        /// Create a new Branch with the projectname
        /// Set the reference to the remote origin and push it there to fill the new branch
        /// </summary>
        /// <param name="gitUrl"></param>
        /// <param name="projectName"></param>
        private void CreateNewRemoteBranch(string gitUrl, string projectName)
        {
            using (var repo = new Repository(gitUrl))
            {
                var localbranch = repo.CreateBranch($"{projectName}");

                Remote remote = repo.Network.Remotes["origin"];

                repo.Branches.Update(localbranch, branch => branch.Remote = remote.Name,
                    branch => branch.UpstreamBranch = localbranch.CanonicalName);
                PushOptions options = new PushOptions
                {
                    CredentialsProvider = (url, fromUrl, types) =>
                        new UsernamePasswordCredentials
                        {
                            Username = Config.GetGitUsername(),
                            Password = Config.GetGitPassword()
                        }
                };

                //  options.OnPushTransferProgress += PushTransferProgress;
                repo.Network.Push(localbranch, options);
                Commands.Checkout(repo, localbranch);
            }
        }



        /// <summary>
        /// Get the logincredentials and use them as options
        /// </summary>
        /// <returns></returns>
        private static CloneOptions GetCloneOptions()
        {
            CloneOptions options = null;

            string username = Config.GetGitUsername();
            string password = Config.GetGitPassword();

            if (!string.IsNullOrWhiteSpace(username) && !string.IsNullOrWhiteSpace(password))
            {
                Credentials credentials = new UsernamePasswordCredentials() { Username = username, Password = password };
                options = new CloneOptions
                {
                    CredentialsProvider = (_url, _user, _cred) => credentials

                };
            }

            return options;
        }

        /// <summary>
        /// Get the logincredentials and use them as options
        /// </summary>
        /// <returns></returns>
        private static FetchOptions GetFetchOptions()
        {
            FetchOptions options = new FetchOptions();
            string username = Config.GetGitUsername();
            string password = Config.GetGitPassword();

            if (!string.IsNullOrWhiteSpace(username) && !string.IsNullOrWhiteSpace(password))
            {
                options.CredentialsProvider = new CredentialsHandler((url, usernameFromUrl, types) =>
                    new UsernamePasswordCredentials()
                    {
                        Username = username,
                        Password = password
                    });
            }
            return options;
        }

        /// <summary>
        /// Get the logincredentials and use them as options
        /// </summary>
        /// <returns></returns>
        private static PushOptions GetPushOptions()
        {
            PushOptions options = new PushOptions();
            string username = Config.GetGitUsername();
            string password = Config.GetGitPassword();

            if (!string.IsNullOrWhiteSpace(username) && !string.IsNullOrWhiteSpace(password))
            {
                options.CredentialsProvider = new CredentialsHandler((url, usernameFromUrl, types) =>
                    new UsernamePasswordCredentials()
                    {
                        Username = username,
                        Password = password
                    });
            }
            return options;
        }

        /// <summary>
        /// Get all Remotebranches in this url we have passed
        /// use the passed options which contain the credentials
        /// </summary>
        /// <param name="gitUrl"></param>
        /// <param name="options"></param>
        /// <returns></returns>
        private static IEnumerable<string> GetBranchesInternal(string gitUrl, CloneOptions options)
        {
            try
            {
                return Repository.ListRemoteReferences(gitUrl, options.CredentialsProvider).Where(elem => elem.IsLocalBranch)
                                                                                               ?.Select(elem => elem.CanonicalName
                                                                                               ?.Replace("refs/heads/", ""));
            }
            catch (Exception ex)
            {
                LogHelper.Instance.Log($@"Error while fetching branches, URL: {gitUrl}", ex);
            }

            return null;
        }

        public static void CommitAndPush(string path, string branchName, string logMessage = "Commit")
        {
            using (var repo = new Repository(path))
            { 
                // Create the committer's signature and commit
                Signature author = new Signature(Config.GetGitUsername(), Config.GetGitUsername()+"@anothermonday.com", DateTime.Now);
                Signature committer = author;

                Commands.Stage(repo, "*");
                // Commit to the repository
                Commit commit = repo.Commit(logMessage, author, committer);

                var options = GetPushOptions();
                if (options!=null)
                {
                    repo.Network.Push(repo.Branches[branchName], options);
                }
            }
        }

        public static bool GetOrUpdateConfigFile(string gitUrl)
        {

            var destination = Environment.CurrentDirectory + $@"\ApplicationConfiguration";
            var gentleGitConfigPath = destination + $@"\GentleGit";

            if (!Config.IsGitConfigCloned() && Helper.IsDirectoryEmpty(destination)) // CLONE THE CONFIG REPO 
            {
                CloneOptions options = GetCloneOptions();
                if (options != null)
                {
                    IEnumerable<string> branches = GetBranchesInternal(gitUrl, options);
                    if (branches != null && branches.Any())
                    {
                        string targetBranchName = branches.FirstOrDefault(i => i.Contains("master"));

                        if (!string.IsNullOrEmpty(targetBranchName))
                        {
                            options.BranchName = targetBranchName;
                            //options.OnTransferProgress = TransferProgress;

                            Repository.Clone(gitUrl, destination, options);
                            var files = Directory.EnumerateFiles(gentleGitConfigPath);
                            foreach (var file in files)
                            {
                                if (file.Contains("Customer"))
                                {
                                    MessageBox.Show(File.ReadAllText(file));
                                    return true;
                                }
                            }

                            Config.SetGitConfigCloned(true);
                        }
                        else
                        {
                            LogHelper.Instance.Log($"GIT repository with URL='{gitUrl}' has no branch containing project with name master");
                        }
                    }
                    else
                    {
                        LogHelper.Instance.Log("Git branches are null or empty.");
                    }
                }
                else
                {
                    LogHelper.Instance.Log("CloneOptions is null, check username and password in App.config.");
                }

            }
            else // UPDATE CONFIG
            {

                FetchOptions options = GetFetchOptions();
                if (options != null)
                {
                    string logMessage = "";
                    using (var repo = new Repository(destination))
                    {
                        var remote = repo.Network.Remotes["origin"];
                        IEnumerable<string> refSpecs = remote.FetchRefSpecs.Select(x => x.Specification);
                        Commands.Fetch(repo, remote.Name, refSpecs, options, logMessage);
                        return true;
                    }
                    LogHelper.Instance.Log(logMessage);
                }
                else
                {
                    LogHelper.Instance.Log("FETCHOptions is null, check username and password in App.config.");
                }
            }

            return false;
        }

        public static List<String> GetCustomersList()
        {

            var destination = Environment.CurrentDirectory + $@"\ApplicationConfiguration";
            var gentleGitConfigPath = destination + $@"\GentleGit";
            var files = Directory.EnumerateFiles(gentleGitConfigPath);
            foreach (var file in files)
            {
                if (file.Contains("Customers"))
                {
                    var result = JsonConvert.DeserializeObject<CustomersClass>(File.ReadAllText(file));
                    return result.Customers;

                }
            }

            return null;
        }

        private bool TransferProgress(TransferProgress progress)
        {
            Console.WriteLine($"Objects: {progress.ReceivedObjects} of {progress.TotalObjects}, Bytes: {progress.ReceivedBytes}");
            ProgressEvent?.Invoke(progress);
            return true;
        }

        private bool PushTransferProgress(int current, int total, long bytes)
        {
            Console.WriteLine($"Push: Current {current} of Total {total}, Bytes: {bytes}");
            PushProgressEvent?.Invoke(current, total, bytes);
            return true;
        }
        #endregion
    }
}
