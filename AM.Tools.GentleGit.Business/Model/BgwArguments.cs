﻿using AM.Tools.GentleGit.Common.Enum;
using System.Collections.Generic;
using System.Security.Policy;

namespace AM.Tools.GentleGit.Business.Model
{
    public class BgwArguments
    {
        public string Url { get; set; }
        public string DestinationPath { get; set; }
        public string ProjectName { get; set; }
        public ProjectSettingsEnum ActiveOption { get; set; }
        public bool CreateNewProjectOnGit { get; set; }
       
        public ProjectSettings ProjectSettings { get; set; }

        public BgwArguments()
        {
            //do nothing
        }

        public BgwArguments(string url, string destinationPath, string projectName, List<ProjectSettingsEnum> options, bool createNewProjectOnGit = false, ProjectSettings projectSettings = null)
        {
            Url = url;
            DestinationPath = destinationPath;
            ProjectName = projectName;
            CreateNewProjectOnGit = createNewProjectOnGit;
            ProjectSettings = projectSettings;
        }
    }
}
