﻿using AM.Tools.GentleGit.Common.Enum;
using System.Collections.Generic;

namespace AM.Tools.GentleGit.Business.Model
{
    public class ProjectSettings
    {
        public string CompanyName { get; set; }
        public string FolderName { get; set; }
        public string ProjectName { get; set; }
        public string ApplicationPath { get; set; }
        public string SvnUrl { get; set; }
        public string GitUrl { get; set; }
        public List<ProjectSettingsEnum> Options { get; set; }
        public bool CreateNewProject { get; set; }
    }
}
